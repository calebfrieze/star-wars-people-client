# Swclient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.23.

## Environment
This project uses docker/docker-compose for its API and Database layers.

To run this project first `npm install`

Make sure docker images are up to date `docker-compose pull`

To "up" the stack - `docker-compose up -d`

This docker configuration can be found in `docker-compose.yml`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
